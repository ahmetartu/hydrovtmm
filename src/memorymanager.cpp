/*
	Project: HydroVTMM
	Description: A User-level virtual memory system for shared-memory multithreaded systems to process large DEMs 

	HydroVTMM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Regarding this entire document or any portion of it , the author 
	makes no guarantees and is not responsible for any damage resulting 
	from its use.

	Ahmet Artu Yildirim
	Utah State University
	ahmetartu@aggiemail.usu.edu
*/

#include "memorymanager.h"
#include "tifio.h"
#include "planchonworker.h"
#include "timing.h"
#include <math.h>
#include <Windows.h>
#include <Psapi.h>
#include <gdal.h>
#include <cpl_conv.h>
#include <cpl_string.h>

#define VERBOSE 0

typedef struct _DEMFile
{
	char* m_tif_file_path;
	int m_width;
	int m_height;
	int m_type;
	double m_nodata;
	float* m_planchon;
	GDALRasterBandH hBand;
	GDALDatasetH hDataset;
} DEMFile;

extern planchonworker** workers;

static tile** m_tiles = NULL;
static ofstream memlogfile;
static int m_maxtilewidth = 100;
static int m_maxtileheight = 100;
static int m_tile_columns;
static int m_tile_rows;
static int m_global_tile_count;
static long long m_memory_limit;
static char* m_swap_path = NULL;
static bool m_enable_mm = true;
static HANDLE m_io_thread_handle;
static DWORD m_io_thread_num;
static volatile HANDLE hIOEvent;
static volatile bool m_swap_out_requested = false;
static DEMFile* m_input_dem_handle;
static DEMFile* m_output_dem_handle;
static double m_fmemlogtime;
static double m_fmemlogstarttime;
static volatile LONG m_timer = 0;
static volatile LONG m_request_count = 0;
static volatile LONG m_hit_count = 0;
static volatile bool m_outputtif_initialized = false;
static volatile int m_thread_count;
static volatile int m_current_worker_for_prefetch = 0;
// disable prefetch
static volatile int m_async_fetch_count = 0;
static CRITICAL_SECTION m_critical_section_swapout;
static const double m_gcollection_ratio = 1.2;
static const int DIV = 1024*1024;
static DWORDLONG m_installedPhysMem;
static HANDLE m_processHandle;
static const bool enableMemUsageLogging = false;

static int swapOut (tile* tile);
static void sendGetTileSignalToIOThread(UINT msg, planchonworker* worker, tile_request* request);
static void sendSignalToIOThread(UINT msg, WPARAM wParam, LPARAM lParam);
static float* internalReadDEMBlock(DEMFile* dem_handle, int x, int y, int width, int height);
static void saveTileToOutputDEM(tile* tile);
static void initializeTiffIO();
static DEMFile* openDEM(const char* tifpath);
static SIZE_T getCurrentMemoryUsage();

void logMemoryUsage() 
{
	if (!enableMemUsageLogging)
		return;

	double memlongstime = getCurrentTimeSecs();
	if (memlongstime - m_fmemlogtime >= 1.0)
	{
		memlogfile << memlongstime-m_fmemlogstarttime << "\t" << getCurrentMemoryUsage() << endl;

		m_fmemlogtime = memlongstime;
	}
}

DWORD WINAPI IOThreadProc(LPVOID n)
{
	MSG msg;
	// call PeekMessage to force for creation of thread message queue
	PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	SetEvent(hIOEvent);

	while(1)
	{
		BOOL msgReturn = GetMessage(&msg ,NULL ,THRD_MESSAGE_INIT, THRD_MESSAGE_EXIT);

		if (msgReturn)
		{
			switch (msg.message)
			{
			case THRD_MESSAGE_INIT:
				GDALAllRegister();
				SetEvent(hIOEvent);
				break;
			case THRD_MESSAGE_OPEN_DEM:
				{
					m_input_dem_handle->hDataset = GDALOpen(m_input_dem_handle->m_tif_file_path, GA_ReadOnly);
					if (m_input_dem_handle->hDataset == NULL ) 
					{
						cerr << "Error in opening " << m_input_dem_handle->m_tif_file_path << endl;
						exit(1);
					}

					char* extension = strchr(m_input_dem_handle->m_tif_file_path,'.');
					char* ext = extension;
					extension++;

					int f_type = FTYPE_UNKNOWN;

					if (strcmp(extension, "tif") == 0 || strcmp(extension, "TIF") == 0 || strcmp(extension, "Tif") == 0)
					{
						f_type = FTYPE_TIFF;
					}
					else if (strcmp(extension, "img") == 0 || strcmp(extension, "IMG") == 0 || strcmp(extension, "Img") == 0)
					{
						f_type = FTYPE_ERDAS;
					}

					if (f_type == FTYPE_UNKNOWN)
					{
						cerr << "Not supported format: " << extension << endl;
						exit(1);
					}

					m_input_dem_handle->m_type = f_type;
					m_input_dem_handle->hBand = GDALGetRasterBand(m_input_dem_handle->hDataset, 1);
					m_input_dem_handle->m_width = GDALGetRasterXSize(m_input_dem_handle->hDataset);
					m_input_dem_handle->m_height = GDALGetRasterYSize(m_input_dem_handle->hDataset);
					m_input_dem_handle->m_nodata = GDALGetRasterNoDataValue(m_input_dem_handle->hBand, NULL);

					m_output_dem_handle = (DEMFile*) malloc(sizeof(DEMFile));
					m_output_dem_handle->m_width = m_input_dem_handle->m_width;
					m_output_dem_handle->m_height = m_input_dem_handle->m_height;
					m_output_dem_handle->m_nodata = m_input_dem_handle->m_nodata;
					m_output_dem_handle->m_type = f_type;

					m_output_dem_handle->m_tif_file_path = (char*) malloc (strlen(m_input_dem_handle->m_tif_file_path) + 3 + 1);
					char* p;
					int i = 0;
					for(p=m_input_dem_handle->m_tif_file_path; p!=ext; p++)
					{
						m_output_dem_handle->m_tif_file_path[i] = *p;
						i++;
					}

					m_output_dem_handle->m_tif_file_path[i] = 'O';i++;
					m_output_dem_handle->m_tif_file_path[i] = 'u';i++;
					m_output_dem_handle->m_tif_file_path[i] = 't';i++;
					m_output_dem_handle->m_tif_file_path[i] = '.';i++;
					int j, k = 0;
					for (j = 0; j < strlen(extension); j++)
					{
						m_output_dem_handle->m_tif_file_path[i] = extension[k];
						k++;
						i++;
					}

					m_output_dem_handle->m_tif_file_path[i] = '\0';

					SetEvent(hIOEvent);
				}
				break;
			case THRD_MESSAGE_GET_TILE:
				{
					planchonworker* worker = (planchonworker*) msg.wParam;
					tile_request* request = (tile_request*) msg.lParam;
					int tileID = request->tileID;
					bool sendprefetchrequest = false;
					if (request->prefetchSize > 0)
					{
						sendprefetchrequest = true;
					}

					tile* myt = m_tiles[tileID];

					InterlockedIncrement(&myt->useCount);
					InterlockedIncrement(&m_request_count);
					InterlockedIncrement(&m_timer);

					logMemoryUsage();

					myt->timeStamp = m_timer;

					if (myt->state != PRESENT)
					{
						bool mmenabled = isMMEnabled();

						if (myt->state == UNINITIALIZED)
						{
							myt->map = (float*) internalReadDEMBlock((DEMFile*) m_input_dem_handle, myt->rx, myt->ry, myt->rwidth, myt->rheight);
							myt->planchon = (float*) malloc(sizeof(float) *  myt->rwidth * myt->rheight);
							myt->state = PRESENT;
						}
						else if (myt->state == SWAPPEDOUT)
						{
							swapIn(myt);
						}
					}
					else
					{
						InterlockedIncrement(&m_hit_count);
					}

					SetEvent(worker->hIOEvent);

					if (sendprefetchrequest)
					{
						for (int i = 0; i < request->prefetchSize; i++) {
							int currentTileID = request->prefetchList[i];

							// if the request is out of bound for the worker, do not fetch!
							if (currentTileID < worker->tilestartindex || currentTileID >= worker->tileendindex)
								continue;

							myt = m_tiles[currentTileID];

							myt->timeStamp = m_timer;
							if (myt->state != PRESENT)
							{						
								if (myt->state == UNINITIALIZED)
								{
									myt->map = (float*) internalReadDEMBlock((DEMFile*) m_input_dem_handle, myt->rx, myt->ry, myt->rwidth, myt->rheight);
									myt->planchon = (float*) malloc(sizeof(float) *  myt->rwidth * myt->rheight);
									myt->state = PRESENT;
								}
								else if (myt->state == SWAPPEDOUT)
								{
									swapIn(myt);
								}			
							}

							currentTileID++;
							if (currentTileID == worker->tileendindex)
								currentTileID = worker->tilestartindex;
						}						
					}

					if (request->prefetchSize > 0)
						free(request->prefetchList);
					free(request);

					if ( isMMEnabled())
					{
						bool doswap = shouldIDoSwapOutOperation();
						if (doswap && !m_swap_out_requested)
						{
							m_swap_out_requested = true;
							PostThreadMessage(m_io_thread_num, THRD_MESSAGE_DO_SWAP_OUT, 0, 0);
						}
					}
				}
				break;
			case THRD_MESSAGE_PREFETCH:
				{
					planchonworker* worker = (planchonworker*) msg.wParam;
					tile_request* request = (tile_request*) msg.lParam;
					bool sendprefetchrequest = false;
					if (request->prefetchSize > 0)
					{
						sendprefetchrequest = true;
					}

					bool mmenabled = isMMEnabled();

					logMemoryUsage();

					if (sendprefetchrequest)
					{
						for (int i = 0; i < request->prefetchSize; i++) {
							int currentTileID = request->prefetchList[i];

							// if the request is out of bound for the worker, do not fetch!
							if (currentTileID < worker->tilestartindex || currentTileID >= worker->tileendindex)
								continue;

							tile* myt = m_tiles[currentTileID];

							myt->timeStamp = m_timer;
							if (myt->state != PRESENT)
							{						
								if (myt->state == UNINITIALIZED)
								{
									myt->map = (float*) internalReadDEMBlock((DEMFile*) m_input_dem_handle, myt->rx, myt->ry, myt->rwidth, myt->rheight);
									myt->planchon = (float*) malloc(sizeof(float) *  myt->rwidth * myt->rheight);
									myt->state = PRESENT;
								}
								else if (myt->state == SWAPPEDOUT)
								{
									swapIn(myt);
								}			
							}

							currentTileID++;
							if (currentTileID == worker->tileendindex)
								currentTileID = worker->tilestartindex;
						}						
					}

					if (request->prefetchSize > 0)
						free(request->prefetchList);
					free(request);

					if ( isMMEnabled())
					{
						bool doswap = shouldIDoSwapOutOperation();
						if (doswap && !m_swap_out_requested)
						{
							m_swap_out_requested = true;
							PostThreadMessage(m_io_thread_num, THRD_MESSAGE_DO_SWAP_OUT, 0, 0);
						}
					}
				}
				break;
			case THRD_MESSAGE_DO_SWAP_OUT:
				{
					logMemoryUsage();
					performSwapOutOperation();
					m_swap_out_requested = false;
				}
				break;
			case THRD_MESSAGE_WRITE_TILE:
				{
					planchonworker* worker = (planchonworker*) msg.wParam;
					tile_request* request = (tile_request*) msg.lParam;
					int tileID = (int) request->tileID;
					free(request);

					tile* myt = m_tiles[tileID];

					logMemoryUsage();

					if (myt->state == FINISHED)
					{
						SetEvent(worker->hIOEvent);
						break;
					}

					InterlockedIncrement(&myt->useCount);
					InterlockedIncrement(&m_timer);

					myt->timeStamp = m_timer;

					if (myt->state == SWAPPEDOUT)
					{
						swapIn(myt);
					}		

					saveTileToOutputDEM(myt);

					myt->state = FINISHED;

					free(myt->map);
					free(myt->planchon);
					free_mm_stack(myt->stacks[0]);
					free_mm_stack(myt->stacks[1]);

					SetEvent(worker->hIOEvent);
				}
				break;
			case THRD_MESSAGE_EXIT:
				GDALClose(m_output_dem_handle->hDataset);
				GDALClose(m_input_dem_handle->hDataset);
				SetEvent(hIOEvent);
				return 0;
			}
		}
	}

	return 0;
}

bool isTileFinished(int tileID)
{
	return m_tiles[tileID]->finished;
}

tile* getTile(planchonworker* worker, tile_request* request)
{
	int tileID = request->tileID;
	boolean return_tile = false;
	EnterCriticalSection(&m_critical_section_swapout);
	tile* myt = m_tiles[tileID];
	worker->lasttileid = tileID;
	if (myt->state == PRESENT)
	{
		InterlockedIncrement(&myt->useCount);
		InterlockedIncrement(&m_request_count);
		InterlockedIncrement(&m_timer);
		InterlockedIncrement(&m_hit_count);
		myt->timeStamp = m_timer;
		return_tile = true;
	}
	LeaveCriticalSection(&m_critical_section_swapout);

	if (return_tile)
	{
		PostThreadMessage(getIOThreadNum(), THRD_MESSAGE_PREFETCH, (WPARAM) worker, (LPARAM) request);
		return m_tiles[tileID];
	}

	sendGetTileSignalToIOThread(THRD_MESSAGE_GET_TILE, worker, request);
	m_tiles[tileID]->state = PRESENT;

	return m_tiles[tileID];
}

void saveTile(planchonworker* worker, int tileID)
{
	tile_request* request = (tile_request*) malloc(sizeof(tile_request));
	request->tileID = tileID;
	request->prefetchSize = 0;
	sendGetTileSignalToIOThread(THRD_MESSAGE_WRITE_TILE, worker, request);
}

bool isMMEnabled()
{
	return m_enable_mm;
}

SIZE_T getCurrentMemoryUsage()
{
	PROCESS_MEMORY_COUNTERS pmc;
	GetProcessMemoryInfo(m_processHandle, &pmc, sizeof(pmc));
	SIZE_T memUsage = (pmc.PagefileUsage / DIV);
#if VERBOSE
	cout << "Current memory usage is " << memUsage << " MB." << endl;
#endif
	return memUsage;
}

DEMFile* initializeMemoryManager(const char* tifpath, int maxtilewidth, int maxtileheight, long long memorylimit, char* swapdirpath, int threadcount)
{
	// enable prefetch
	m_async_fetch_count = 0;

	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof (statex);
	GlobalMemoryStatusEx (&statex);
	m_installedPhysMem = statex.ullTotalPhys/DIV;

#if VERBOSE
	cout << "Installed Memory: " << m_installedPhysMem << " MB." << endl;
#endif

	m_processHandle = GetCurrentProcess();

	if (enableMemUsageLogging) {
		char outputmemlogfilename[260];
		sprintf(outputmemlogfilename, "F%s_T%d_TC%d_M%d_MemLog.txt", tifpath, maxtilewidth, threadcount, memorylimit);

		memlogfile.open (outputmemlogfilename);
	}

	m_fmemlogstarttime = m_fmemlogtime = getCurrentTimeSecs();

	InitializeCriticalSection(&m_critical_section_swapout);

	initializeTiffIO();

	DEMFile* dem_handle = openDEM(tifpath);

	if (memorylimit == -1)
	{
		m_enable_mm = false;
#if VERBOSE
		cout << "Virtual memory management is off" << endl;
#endif
	}
	else
	{
		m_enable_mm = true;
		m_memory_limit = memorylimit;
#if VERBOSE
		cout << "Memory Limit: " << m_memory_limit << " MB." << endl;
#endif
	}

	if (m_enable_mm)
	{
		m_swap_path = (char*) malloc(MAX_PATH);
		strcpy(m_swap_path, swapdirpath);
		int templen = strlen(m_swap_path);

		if (m_swap_path[templen] != '\\')
		{
			m_swap_path[templen] = '\\';
			m_swap_path[templen + 1] = '\0';
		}
	}

	m_maxtilewidth = maxtilewidth;
	m_maxtileheight = maxtileheight;

	const int dem_width = getDEMWidth(dem_handle);
	const int dem_height = getDEMHeight(dem_handle);

	if (dem_width < m_maxtilewidth)
		m_maxtilewidth = dem_width;

	if (dem_height < m_maxtileheight)
		m_maxtileheight = dem_height;

	m_tile_columns = (int) ceil((float)dem_width / (float)m_maxtilewidth);
	m_tile_rows = (int) ceil((float)dem_height / (float)m_maxtileheight);
	m_global_tile_count = m_tile_rows * m_tile_columns;
	m_thread_count = threadcount;

	m_tiles = (tile**) malloc(sizeof(tile*) * m_global_tile_count);

	int c, r, i = 0;
	for (r = 0; r < m_tile_rows; r++)
	{
		for (c = 0; c < m_tile_columns; c++)
		{
			tile* myt = (tile*) malloc(sizeof(tile));
			myt->id = i;
			myt->columnid = c;
			myt->rowid = r;
			myt->x = c * m_maxtilewidth;
			myt->y = r * m_maxtileheight;
			if (c == m_tile_columns - 1)
			{
				myt->width = dem_width - ((m_tile_columns - 1) * m_maxtilewidth);
			}
			else
			{
				myt->width = m_maxtilewidth;
			}

			if (r == m_tile_rows - 1)
			{
				myt->height = dem_height - ((m_tile_rows - 1) * m_maxtileheight);
			}
			else
			{
				myt->height = m_maxtileheight;
			}

			myt->rx = myt->x;
			myt->ry = myt->y;
			myt->rwidth = myt->width;
			myt->rheight = myt->height;

			if (myt->columnid != 0)
			{
				myt->rx = myt->rx - 1;
				myt->rwidth = myt->rwidth + 1;
			}

			if (myt->rowid != 0)
			{
				myt->ry = myt->ry - 1;
				myt->rheight = myt->rheight + 1;
			}

			if (myt->columnid != (m_tile_columns - 1))
			{
				myt->rwidth = myt->rwidth + 1;
			}

			if (myt->rowid != (m_tile_rows - 1))
			{
				myt->rheight = myt->rheight + 1;
			}

			myt->map = NULL;
			myt->planchon = NULL;
			myt->whichstack = 0;
			myt->stacks[0] =  create_mm_stack();
			myt->stacks[1] =  create_mm_stack();
			myt->finished = false;
			myt->useCount = 0;
			myt->timeStamp = 0;

			myt->state = UNINITIALIZED;

			m_tiles[i] = myt;

			i++;
		}
	}

	return dem_handle;
}

tile** getTiles()
{
	return m_tiles;
}

void performSwapOutOperation()
{
	EnterCriticalSection(&m_critical_section_swapout);

	// LRU Algorithm
	// TODO: For small number of tiles, the linear algorithm below is OK but for larger number of tiles, 
	// implement the algorithm using hash and queue data structures for better efficiency
	int i = 0;
	unsigned int smallesttimestamp;
	tile* leastUsedPage;
	while (true)
	{
		smallesttimestamp = UINT_MAX;
		leastUsedPage = NULL;
		for (i = 0; i < m_global_tile_count; i++)
		{
			if (m_tiles[i]->useCount == 0 && m_tiles[i]->state == PRESENT && m_tiles[i]->timeStamp < smallesttimestamp)
			{
				leastUsedPage = m_tiles[i];
				smallesttimestamp = m_tiles[i]->timeStamp;
			}
		}

		if (leastUsedPage) 
		{
			swapOut(leastUsedPage);
		}
		else
		{
			break;
		}
		SIZE_T memusage = getCurrentMemoryUsage();
		memusage = (SIZE_T) (memusage * m_gcollection_ratio);
		if (memusage <= m_memory_limit)
			break;
	}

	LeaveCriticalSection(&m_critical_section_swapout);
}

void swapIn (tile* tile)
{
	if (tile->state != SWAPPEDOUT)
		return;

	int dsize = sizeof(float) * tile->rwidth * tile->rheight;
	tile->map = (float*) malloc(dsize);
	tile->planchon = (float*) malloc(dsize);

	fstream m_swap_file;
	char swapfilename[MAX_PATH];
	sprintf(swapfilename, "%spagefile%d", m_swap_path, tile->id);
	m_swap_file.open(swapfilename, ios::in | ios::binary);
	if (m_swap_file.fail())
	{
		cerr << "Failed to open swap file! SwapFile: " << swapfilename << endl;
		exit(1);
	}

	m_swap_file.read ((char*) tile->map, dsize);
	m_swap_file.read ((char*) tile->planchon, dsize);
	deserialize_mm_stack(tile->stacks[tile->whichstack], &m_swap_file);
	m_swap_file.close();

	tile->state = PRESENT;
}

static int swapOut (tile* tile)
{
	if (tile->state != PRESENT)
		return 0;

	int memsize = 0;
	fstream m_swap_file;
	char swapfilename[MAX_PATH];
	sprintf(swapfilename, "%spagefile%d", m_swap_path, tile->id);

	/* if there is one giant swap file use ios::out | ios::in | ios::binary */
	m_swap_file.open(swapfilename, ios::out | ios::binary);
	if (m_swap_file.fail())
	{
		cerr << "Failed to open swap file! SwapFile: " << swapfilename << endl;
		exit(1);
	}

	int dsize = sizeof(float) * tile->rwidth * tile->rheight;
	m_swap_file.write ((char*) tile->map, dsize);
	m_swap_file.write ((char*) tile->planchon, dsize);
	memsize = dsize + dsize + tile->stacks[tile->whichstack]->capacity;
	serialize_mm_stack(tile->stacks[tile->whichstack], &m_swap_file);

	m_swap_file.flush ();
	m_swap_file.close();

	free(tile->map);
	free(tile->planchon);
	free_mm_stack_data(tile->stacks[(tile->whichstack + 1) % 2]);

	tile->map = NULL;
	tile->planchon = NULL;

	tile->state = SWAPPEDOUT;

	return memsize;
}

static void sendGetTileSignalToIOThread(UINT msg, planchonworker* worker, tile_request* request)
{
	PostThreadMessage(getIOThreadNum(), msg, (WPARAM) worker, (LPARAM) request);
	WaitForSingleObject(worker->hIOEvent, INFINITE);
}

bool shouldIDoSwapOutOperation()
{
	SIZE_T totalPhysMem = getCurrentMemoryUsage();
	if (totalPhysMem > m_memory_limit)
	{
#if VERBOSE
		cout << "Perform swap out operation." << endl;
#endif
		return true;
	}
	return false;
}

void finalizeMemoryManager()
{
	if (enableMemUsageLogging)
		memlogfile.close();

	finalizeTiffIO();
}

int getTileColumnSize()
{
	return m_tile_columns;
}

int getTileRowSize()
{
	return m_tile_rows;
}

int getTileSize()
{
	return m_global_tile_count;
}

int getMaxTileWidth()
{
	return m_maxtilewidth;
}

int getMaxTileHeight()
{
	return m_maxtileheight;
}

static void sendSignalToIOThread(UINT msg, WPARAM wParam, LPARAM lParam)
{
	PostThreadMessage(m_io_thread_num, msg, wParam, lParam);
	WaitForSingleObject(hIOEvent, INFINITE);
}

void finalizeTiffIO()
{
	PostThreadMessage(m_io_thread_num, THRD_MESSAGE_EXIT, 0, 0);
	WaitForSingleObject(hIOEvent, INFINITE);
}

DWORD getIOThreadNum()
{
	return m_io_thread_num;
}

void initializeTiffIO()
{
	hIOEvent = CreateEvent(NULL, false, false, NULL);
	// create io thread and send message to io thread to initialize DEM input
	m_io_thread_handle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)IOThreadProc, 
		(LPVOID)0 , 0, &m_io_thread_num);
	WaitForSingleObject(hIOEvent, INFINITE);

	sendSignalToIOThread(THRD_MESSAGE_INIT, 0, 0);
}

DEMFile* openDEM(const char* tifpath)
{
	m_input_dem_handle = (DEMFile*) malloc(sizeof(DEMFile));
	m_input_dem_handle->m_tif_file_path = NULL;
	m_input_dem_handle->m_width = 0;
	m_input_dem_handle->m_height = 0;
	m_input_dem_handle->m_nodata;
	m_input_dem_handle->m_planchon = NULL;

	m_input_dem_handle->m_tif_file_path = (char*) malloc (strlen(tifpath) + 1);
	strcpy(m_input_dem_handle->m_tif_file_path, tifpath);

	sendSignalToIOThread(THRD_MESSAGE_OPEN_DEM, 0, 0);

	return m_input_dem_handle;
}

float* internalReadDEMBlock(DEMFile* dem_handle, int x, int y, int width, int height)
{
	float* blockbuffer = (float *) malloc(sizeof(float) * width * height);
	GDALRasterIO(dem_handle->hBand, GF_Read, x, y, width, height, blockbuffer, width, height, GDT_Float32, 0, 0);
	return blockbuffer;
}

double getHitRatio()
{
	return (double) m_hit_count / (double) m_request_count;
}

void saveTileToOutputDEM(tile* tile)
{
	if (m_outputtif_initialized == false)
	{
		char *pszFormat = "GTiff";
		if (m_output_dem_handle->m_type == FTYPE_ERDAS)
		{
			pszFormat = "HFA";
		}

		GDALDriverH hDriver = GDALGetDriverByName( pszFormat );
		char **papszMetadata;

		if( hDriver == NULL )
			exit(1);

		papszMetadata = GDALGetMetadata(hDriver, NULL);
		if( !CSLFetchBoolean( papszMetadata, GDAL_DCAP_CREATE, FALSE))
		{
			cout << "driver " << pszFormat << " does not support Create() method." << endl;
			exit(1);
		}

		if(!CSLFetchBoolean(papszMetadata, GDAL_DCAP_CREATECOPY, FALSE))
		{
			cout << "driver " << pszFormat << " does not support CreateCopy() method." << endl;
			exit(1);
		}

		m_output_dem_handle->hDataset = GDALCreateCopy(hDriver, m_output_dem_handle->m_tif_file_path, m_input_dem_handle->hDataset, FALSE, NULL, NULL, NULL);

		m_output_dem_handle->hBand = GDALGetRasterBand(m_output_dem_handle->hDataset, 1);

		m_outputtif_initialized = true;
	}

	int maxtilewidth = getMaxTileWidth();
	int maxtileheight = getMaxTileHeight();
	int ystart = (tile->y - tile->ry);
	int	xstart = (tile->x - tile->rx);
	int	xfile = (tile->columnid * maxtilewidth);
	int	yfile = (tile->rowid * maxtileheight);
	int width = m_input_dem_handle->m_width;
	int height = m_input_dem_handle->m_height;
	int i;

	float* tiledata = (float*) malloc(sizeof(float) * tile->width * tile->height);
	for (i = 0; i < tile->height; i++)
	{
		memcpy((void*)(float*)(tiledata + (tile->width * i)), (void*)((float*)tile->planchon + xstart + (tile->rwidth * (i + ystart))), tile->width * sizeof(float));
	}

	GDALRasterIO(m_output_dem_handle->hBand, GF_Write, xfile, yfile, tile->width, tile->height, tiledata, tile->width, tile->height, GDT_Float32, 0, 0 );  

	free(tiledata);
}

int getDEMWidth(DEMFile* dem_handle)
{
	return dem_handle->m_width;
}

int getDEMHeight(DEMFile* dem_handle)
{
	return dem_handle->m_height;
}

float* getPlanchonBuffer(DEMFile* dem_handle)
{
	return dem_handle->m_planchon;
}

float getDEMNoData(DEMFile* dem_handle)
{
	return (float) dem_handle->m_nodata;
}
