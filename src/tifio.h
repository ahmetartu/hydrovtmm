/*
	Project: HydroVTMM
	Description: A User-level virtual memory system for shared-memory multithreaded systems to process large DEMs 

	HydroVTMM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Regarding this entire document or any portion of it , the author 
	makes no guarantees and is not responsible for any damage resulting 
	from its use.

	Ahmet Artu Yildirim
	Utah State University
	ahmetartu@aggiemail.usu.edu
*/

#ifndef __TIFIO__
#define __TIFIO__

#include <Windows.h>
#include <iostream>
#include "tile.h"

using namespace std;

typedef struct _DEMFile DEMFile;

#define THRD_MESSAGE_INIT			WM_USER + 1
#define THRD_MESSAGE_OPEN_DEM		WM_USER + 2
#define THRD_MESSAGE_GET_TILE		WM_USER + 3
#define THRD_MESSAGE_DO_SWAP_OUT	WM_USER + 4
#define THRD_MESSAGE_WRITE_TILE 	WM_USER + 5
#define THRD_MESSAGE_PREFETCH   	WM_USER + 6
#define THRD_MESSAGE_EXIT			WM_USER + 50

#define FTYPE_UNKNOWN	0
#define FTYPE_TIFF		1
#define FTYPE_ERDAS		2

#endif