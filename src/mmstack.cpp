/*
	Project: HydroVTMM
	Description: A User-level virtual memory system for shared-memory multithreaded systems to process large DEMs 

	HydroVTMM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Regarding this entire document or any portion of it , the author 
	makes no guarantees and is not responsible for any damage resulting 
	from its use.

	Ahmet Artu Yildirim
	Utah State University
	ahmetartu@aggiemail.usu.edu
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mmstack.h"

mmstack* create_mm_stack ()
{
	mmstack* stack = (mmstack*) malloc (sizeof (mmstack));
	if (!stack)
	{
		cerr << "mmstack: Memory allocation error!" << endl;
		return NULL;
	}

	stack->data = (int*) malloc (INITIAL_CAPACITY * sizeof (int));
	if (!stack->data)
	{
		free (stack);
		cerr << "mmstack: Memory allocation error!" << endl;
		return NULL;
	}
	stack->size = 0;
	stack->capacity = INITIAL_CAPACITY;

	return stack;
}

void pop_mm_stack (mmstack* stack)
{
	if (!stack || stack->size == 0)
		return;

	stack->size--;

	//NOTE: implement the stack shrinking if the size is below some level to save more memory
}

int	top_mm_stack (mmstack* stack)
{
	if (!stack || stack->size == 0)
	{
		cerr << "mmstack: top is empty!" << endl;
		return -1;
	}

	return *(stack->data + (stack->size - 1));
}

void push_mm_stack (mmstack* stack, int item)
{
	if (stack->data == NULL) {
		stack->data = (int*) malloc (INITIAL_CAPACITY * sizeof (int));
		stack->capacity = INITIAL_CAPACITY;
		stack->size = 0;
	}

	// increase the capacity of stack if we hit the capacity
	if (stack->size == stack->capacity)
	{
		int newcapacity = (int) stack->size * GROW_CONSTANT;
		int* newdata = (int*) realloc ((void*) stack->data, newcapacity * sizeof (int));
		if (!newdata)
		{
			cerr << "mmstack: Memory allocation error!" << endl;
			return;
		}
		
		stack->data = newdata;
		stack->capacity = newcapacity;
	}

	*(stack->data + stack->size) = item;
	stack->size++;
}

bool is_mm_stack_empty (mmstack* stack)
{
	if (stack->size != 0)
		return false;

	return true;
}

int	size_mm_stack(mmstack* stack)
{
	return stack->size;
}

void free_mm_stack (mmstack* stack)
{
	if (!stack)
		return;

	if (stack->data)
		free (stack->data);

	free (stack);
}

void free_mm_stack_data(mmstack* stack)
{
	if (!stack)
	{
		cerr << "mmstack: null pointer" << endl;
		return;
	}

	free(stack->data);
	stack->data = NULL;
	stack->size = 0;
	stack->capacity = 0;
}

void serialize_mm_stack (mmstack* stack, fstream* m_swap_file)
{
	if (!stack || !m_swap_file)
	{
		cerr << "mmstack: null pointer" << endl;
		return;
	}

	int dsize = sizeof(int) * stack->size;
	m_swap_file->write ((char*) &stack->size, sizeof(int));

	if (stack->size != 0)
	{
		m_swap_file->write ((char*) stack->data, dsize);
	}

	free(stack->data);
	stack->data = NULL;
	stack->size = 0;
	stack->capacity = 0;
}

void deserialize_mm_stack (mmstack* stack, fstream* m_swap_file)
{
	if (!stack || !m_swap_file)
	{
		cerr << "mmstack: null pointer!" << endl;
		return;
	}

	if (stack->data != NULL)
		return;

	int size;
	m_swap_file->read ((char*) &size, sizeof(int));

	int* data = NULL;

	if (size != 0)
	{
		data = (int*) malloc(sizeof(int) * size);
		m_swap_file->read ((char*) data, size * sizeof(int));
		stack->capacity = size;
	} 
	else
	{
		data = (int*) malloc (INITIAL_CAPACITY * sizeof (int));
		stack->capacity = INITIAL_CAPACITY;
	}
		
	stack->data = data;
	stack->size = size;
}
