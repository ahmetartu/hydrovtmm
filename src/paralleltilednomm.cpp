/*
	Project: HydroVTMM
	Description: A User-level virtual memory system for shared-memory multithreaded systems to process large DEMs 

	HydroVTMM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Regarding this entire document or any portion of it , the author 
	makes no guarantees and is not responsible for any damage resulting 
	from its use.

	Ahmet Artu Yildirim
	Utah State University
	ahmetartu@aggiemail.usu.edu
*/

#include "paralleltilednomm.h"
#include "tile.h"
#include "timing.h"
#include "planchonworker.h"
#include "memorymanager.h"
#include "tifio.h"

#include <fstream>
using namespace std;

static int thread_count;

planchonworker** workers = NULL;

static volatile long num_of_threads_that_entered_barrier = 0;
static volatile long num_of_threads_that_exited_barrier = 0;
static volatile long num_of_threads_that_entered_barrier2 = 0;
static volatile long num_of_threads_that_exited_barrier2 = 0;
static volatile long num_of_threads_that_entered_barrier3 = 0;
static volatile long num_of_threads_that_exited_barrier3 = 0;
static volatile long num_of_threads_that_entered_barrier4 = 0;
static volatile long num_of_threads_that_exited_barrier4 = 0;
static HANDLE entrance_semaphore = NULL;
static HANDLE exit_semaphore = NULL;
static HANDLE entrance_semaphore2 = NULL;
static HANDLE exit_semaphore2 = NULL;
static HANDLE entrance_semaphore3 = NULL;
static HANDLE exit_semaphore3 = NULL;
static HANDLE entrance_semaphore4 = NULL;
static HANDLE exit_semaphore4 = NULL;
static const int MAX_TILE_SIZE = 1000;
static const float epsilon = 0.00000001f;
static const bool enableLogging = false;

static HANDLE* all_threads = NULL;
static volatile bool globalfinished = true;
static volatile bool globalfinished2 = true;
static DEMFile* dem_file = NULL;
static double ioTime = 0.0;
static __int64* tileWorkloads = NULL;
static bool enableLoadBalancing = false;

static bool hasAccess(tile* myt, int x, int y);
static void runPlanchonWorkers();
static void writeTileResult();
static void exchangeBorders(int threadid);
static void createTiles(int threadid);
static void createTiles();
static void initializePlanchon(int threadid);
static void processPlanchon(int threadid);
static void waitForCompletionOfAllWorkers();
static void writeLocalTileResults(int threadid);
static void saveAllTiles(int threadid);
static DWORD WINAPI planchonWorkerProc(LPVOID lpParam);
static void performLoadBalancing(int threadid);
static int removeDirectory(LPCWSTR dir);

timeStamp startIOTime()
{
	return getCurrentTimeStamp();
}

void stopIOTime(timeStamp tStart)
{

	timeStamp tEnd = getCurrentTimeStamp();
	double diff = getTimeSecs(tStart, tEnd);
	ioTime += diff;
}

static int removeDirectory(LPCWSTR dir)
{
	int len = wcslen (dir) + 2;
	wchar_t* tempdir = (wchar_t*) malloc(len * sizeof(wchar_t));
	wmemset(tempdir,0,len);
	lstrcpyW(tempdir,dir);

	SHFILEOPSTRUCT file_op = {
		NULL,
		FO_DELETE,
		tempdir,
		L"",
		FOF_NOCONFIRMATION |
		FOF_NOERRORUI |
		FOF_SILENT,
		false,
		0,
		L"" };
	int ret = SHFileOperation(&file_op);
	free(tempdir);
	return ret;
}

int main(int argc, char* argv[])
{
	entrance_semaphore = CreateSemaphore(NULL,0,4096,NULL);
	exit_semaphore = CreateSemaphore(NULL,0,4096,NULL);

	entrance_semaphore2 = CreateSemaphore(NULL,0,4096,NULL);
	exit_semaphore2 = CreateSemaphore(NULL,0,4096,NULL);

	entrance_semaphore3 = CreateSemaphore(NULL,0,4096,NULL);
	exit_semaphore3 = CreateSemaphore(NULL,0,4096,NULL);

	entrance_semaphore4 = CreateSemaphore(NULL,0,4096,NULL);
	exit_semaphore4 = CreateSemaphore(NULL,0,4096,NULL);

	int tilewidth;
	int tileheight;
	long long memorylimit;
	char* swapdirpath = "SwapDir";
	char* inputdataset;
	const int defaultTileSize = 100;

	if (argc == 2)
	{
		// usage: program.exe inputdataset
		inputdataset = argv[1];
		thread_count = 4;
		tilewidth = defaultTileSize;
		tileheight = defaultTileSize;
		memorylimit = -1;
	}
	else if (argc == 3)
	{
		// usage: program.exe inputdataset threadcount
		inputdataset = argv[1];
		thread_count = atoi(argv[2]);
		tilewidth = defaultTileSize;
		tileheight = defaultTileSize;
		memorylimit = -1;
	}
	else if (argc == 4)
	{
		// usage: program.exe inputdataset threadcount tilesize memorylimitinmb
		inputdataset = argv[1];
		thread_count = atoi(argv[2]);
		int tilesize = atoi(argv[3]);
		tilewidth = tilesize;
		tileheight = tilesize;
		memorylimit = -1;
	}
	else if (argc == 5)
	{
		// usage: program.exe inputdataset threadcount tilesize memorylimitinmb
		inputdataset = argv[1];
		thread_count = atoi(argv[2]);
		int tilesize = atoi(argv[3]);
		tilewidth = tilesize;
		tileheight = tilesize;
		int memlimitinmb = atoi (argv[4]);
		memorylimit = memlimitinmb;
	}
	else
	{
		cout << "usage: program.exe inputdataset threadcount(optional) tilesize(optional) memorylimitinmb(optional)" << endl;
		return 1;
	}

	wchar_t wcmd[40];
	mbstowcs(wcmd,swapdirpath,strlen(swapdirpath) + 1);
	removeDirectory(wcmd);
	CreateDirectory( wcmd, NULL);
	SetFileAttributes(wcmd, FILE_ATTRIBUTE_HIDDEN);

	dem_file = initializeMemoryManager(inputdataset, tilewidth, tileheight, memorylimit, swapdirpath, thread_count);

	timeStamp totalstart = getCurrentTimeStamp();

	runPlanchonWorkers();

	timeStamp additionalIOTime = startIOTime();

	waitForCompletionOfAllWorkers();

	stopIOTime(additionalIOTime);

	timeStamp totalstop = getCurrentTimeStamp();

	double totalexecutiontime = getTimeSecs(totalstart, totalstop);
	double algorithmexecutiontime = totalexecutiontime - ioTime;

	finalizeMemoryManager();

	if (enableLogging) {
		// log statistics
		char outputfilename[260];
		sprintf(outputfilename, "F%s_T%d_TC%d_M%d.txt", inputdataset, tilewidth, thread_count, memorylimit);

		ofstream myfile;
		myfile.open (outputfilename);

		myfile << "Dataset: " << inputdataset << endl;
		myfile << "Tile Size: " << tilewidth << endl;
		myfile << "Thread Count: " << thread_count << endl;
		myfile << "Memory Limit: " << memorylimit << endl;
		myfile << "Total Elapsed Time: " << totalexecutiontime << endl;
		//NOTE: algorithm time affected by sync. time, to measure it correctly run the program with the disabled memory limit
		myfile << "Algorithm Execution Time: " << algorithmexecutiontime << endl;
		myfile << "IO Time: " << ioTime << endl;
		myfile << "Hit Ratio: " << getHitRatio() << endl << endl;

		myfile.close();
	}

	removeDirectory(wcmd);

	return 0;
}

static void waitForCompletionOfAllWorkers()
{
	if (thread_count == 1)
		return;

	WaitForMultipleObjects(thread_count - 1, all_threads, TRUE, INFINITE);
}

static tile_request* createTileRequestForPitRemove(int tileID) {
	tile_request* request = (tile_request*) malloc(sizeof(tile_request));
	request->tileID = tileID;
	request->prefetchSize = 1;
	request->prefetchList = (int*) malloc(sizeof(int));

	// prefetch the next tile
	request->prefetchList[0] = tileID + 1;
	return request;
}

static void initializePlanchon(int threadid)
{
	const int d1[9] = { 0,1, 1, 0,-1,-1,-1,0,1};
	const int d2[9] = { 0,0,-1,-1,-1, 0, 1,1,1};
	planchonworker* self = workers[threadid];

	int j,i,k,t,ystart,yend,xstart,xend, ct;
	long in,jn;
	bool con=false;
	const int tile_columns = getTileColumnSize();
	const int tile_rows = getTileRowSize();
	const float nodata = getDEMNoData(dem_file);
	timeStamp tempiotime;

	long tilestartindex = workers[threadid]->tilestartindex;
	long tileendindex =  workers[threadid]->tileendindex;

	for (t = tilestartindex; t < tileendindex; t++)
	{
		if (threadid == 0)
			tempiotime = startIOTime();
		tile* myt = getTile(self, createTileRequestForPitRemove(t));
		if (threadid == 0)
			stopIOTime(tempiotime);

		ystart = (myt->y - myt->ry);
		yend = ystart + myt->height;
		xstart = (myt->x - myt->rx);
		xend = xstart + myt->width;

		for(j = ystart; j<yend; j++)
		{
			for(i = xstart; i < xend; i++)
			{
				if (myt->map[j * myt->rwidth + i] == nodata)
				{
					myt->planchon[j * myt->rwidth + i] = nodata;
				}
				else if ((i == 0 && myt->columnid == 0) || (j == 0 && myt->rowid == 0) || 
					(i == myt->rwidth - 1 && myt->columnid == tile_columns - 1) ||
					(j == myt->rheight - 1 && myt->rowid == tile_rows - 1))
				{
					myt->planchon[j * myt->rwidth + i] = myt->map[j * myt->rwidth + i];
				}
				else
				{
					con = false;
					for(k=1; k<=8 && !con; k+=1) {
						in = i+d1[k];
						jn = j+d2[k];
						if (myt->map[j * myt->rwidth + i] == nodata)
							con = true;
					}
					if(con)
					{
						myt->planchon[j * myt->rwidth + i] = myt->map[j * myt->rwidth + i];
					}
					else
					{
						myt->planchon[j * myt->rwidth + i] = FLT_MAX;
						push_mm_stack(myt->stacks[myt->whichstack], i);
						push_mm_stack(myt->stacks[myt->whichstack], j);
					}
				}
			}
		}

		tileWorkloads[t] = size_mm_stack(myt->stacks[0]) + size_mm_stack(myt->stacks[1]);
		unlockTile(myt);
	}

	exchangeBorders(threadid);

	performLoadBalancing(threadid);
}

static void performLoadBalancing(int threadid)
{
	if (thread_count == 1)
	{
		return;
	}

	LONG mprev;

	if (InterlockedIncrement(&num_of_threads_that_entered_barrier4)<thread_count)
	{
		WaitForSingleObject(entrance_semaphore4,INFINITE);
	}
	else 
	{ 
		num_of_threads_that_exited_barrier4 = 0;

		int i,j;

		__int64 k;
		int tsize = getTileSize();
		__int64 kt = 0;
		// perform load balancing
		for (i = 0; i < tsize; i++)
		{
			kt += tileWorkloads[i];
		}
		__int64 ave = kt / thread_count;
		i = 0;
		int tstart,tstop;
		tstart = 0;
		tstop = 0;
		__int64 prev;
		int previndex;
		__int64 spoint;
		__int64 stotal = 0;

		for (j = 0; j < thread_count; j++)
		{
			kt = 0;
			prev = 0;
			tstart = tstop;
			spoint = ave * (j+1);
			while(i < tsize)
			{
				if (i > 0 && tileWorkloads[i-1] != 0)
				{
					prev = tileWorkloads[i-1];
					previndex = i-1;
				}

				kt += tileWorkloads[i];
				stotal += tileWorkloads[i];

				if (stotal >= spoint)
				{
					int adiff = stotal - spoint;
					int bdiff = spoint - prev;
					if (prev == 0)
					{
						if (i > 0)
							previndex = i - 1;
						else
							previndex = 0;
					} 

					if (adiff <= bdiff)
					{
						tstop = i + 1;
					}
					else
					{
						tstop = previndex + 1;
					}

					i++;
					break;
				}

				i++;
			}

			workers[j]->tilestartindex = tstart;
			if (j == thread_count - 1)
				tstop = tsize;
			workers[j]->tileendindex = tstop;
			workers[j]->size = tstop - tstart;
		}

		/*
		// test for the effectiveness of the load balance feature
		for (j = 0; j < thread_count; j++)
		{
		__int64 totalelementsinstacks = 0;
		for (i = workers[j]->tilestartindex; i < workers[j]->tileendindex; i++)
		{
		totalelementsinstacks += tileWorkloads[i];
		}
		cout << "T" << j << ":\t" << totalelementsinstacks << endl;
		}

		cout << endl;
		*/

		// let other threads go
		ReleaseSemaphore(entrance_semaphore4,thread_count-1,&mprev);
	}
}

static void writeLocalTileResults(int threadid)
{
	int t, i, xstart, ystart, xfile, yfile;
	const int maxtilewidth = getMaxTileWidth();
	const int maxtileheight = getMaxTileHeight();
	const int width = getDEMWidth(dem_file);
	const int height = getDEMHeight(dem_file);
	float* planchon = getPlanchonBuffer(dem_file);
	timeStamp tempiotime;
	planchonworker* self = workers[threadid];

	long tilestartindex = workers[threadid]->tilestartindex;
	long tileendindex =  workers[threadid]->tileendindex;
	for (t = tilestartindex; t < tileendindex; t++)
	{
		if (threadid == 0)
			tempiotime = startIOTime();
		tile* myt = getTile(self, createTileRequestForPitRemove(t));
		if (threadid == 0)
			stopIOTime(tempiotime);

		ystart = (myt->y - myt->ry);
		xstart = (myt->x - myt->rx);
		xfile = (myt->columnid * maxtilewidth);
		yfile = (myt->rowid * maxtileheight);

		for (i = 0; i < myt->height; i++)
		{
			memcpy(planchon + (yfile * width + xfile), myt->planchon + (ystart * myt->rwidth + xstart), myt->width * sizeof(float));
			ystart++;
			yfile++;
		}
	}
}

static void saveAllTiles(int threadid)
{
	planchonworker* self = workers[threadid];
	long tilestartindex = workers[threadid]->tilestartindex;
	long tileendindex =  workers[threadid]->tileendindex;
	int t, ct;
	timeStamp tempiotime;

	for (t = tilestartindex; t < tileendindex; t++)
	{
		if (threadid == 0)
			tempiotime = startIOTime();
		saveTile(self, t);
		if (threadid == 0)
			stopIOTime(tempiotime);
	}
}

static void exchangeBorders(int threadid)
{
	LONG prev;
	const int tile_columns = getTileColumnSize();
	const int tile_rows = getTileRowSize();
	planchonworker* self = workers[threadid];
	timeStamp tempiotime;

	if (InterlockedIncrement(&num_of_threads_that_entered_barrier)<thread_count)
		WaitForSingleObject(entrance_semaphore,INFINITE);
	else { 
		num_of_threads_that_exited_barrier = 0;
		ReleaseSemaphore(entrance_semaphore,thread_count-1,&prev);
	}

	int j,t,ystart,netystart,xstart,netxstart, ct;
	long tilestartindex = workers[threadid]->tilestartindex;
	long tileendindex =  workers[threadid]->tileendindex;

	for (t = tilestartindex; t < tileendindex; t++)
	{
		if (isTileFinished(t))
		{
			continue;
		}

		if (threadid == 0)
			tempiotime = startIOTime();
		tile* myt = getTile(self, createTileRequestForPitRemove(t));
		if (threadid == 0)
			stopIOTime(tempiotime);

		if (myt->columnid != 0)
		{
			if (threadid == 0)
				tempiotime = startIOTime();
			tile* net = getTile(self, createTileRequestForPitRemove(t - 1));
			if (threadid == 0)
				stopIOTime(tempiotime);

			netystart = (net->y - net->ry);
			netxstart = (net->x - net->rx) + net->width - 1;

			ystart = (myt->y - myt->ry);
			xstart = 0;

			for (j = 0; j < net->height; j++)
			{
				myt->planchon[ystart * myt->rwidth + xstart] = net->planchon[netystart * net->rwidth + netxstart];
				netystart++;
				ystart++;
			}

			unlockTile(net);
		}

		if (myt->columnid != tile_columns - 1)
		{
			if (threadid == 0)
				tempiotime = startIOTime();
			tile* net = getTile(self, createTileRequestForPitRemove(t + 1));
			if (threadid == 0)
				stopIOTime(tempiotime);

			netystart = (net->y - net->ry);
			netxstart = (net->x - net->rx);

			ystart = (myt->y - myt->ry);
			xstart = (myt->x - myt->rx) + myt->width;

			for (j = 0; j < net->height; j++)
			{
				myt->planchon[ystart * myt->rwidth + xstart] = net->planchon[netystart * net->rwidth + netxstart];
				netystart++;
				ystart++;
			}

			unlockTile(net);
		}

		if (myt->rowid != 0)
		{
			if (threadid == 0)
				tempiotime = startIOTime();
			tile* net = getTile(self, createTileRequestForPitRemove(t - tile_columns));
			if (threadid == 0)
				stopIOTime(tempiotime);

			netystart = (net->y - net->ry) + net->height - 1;
			netxstart = (net->x - net->rx);

			ystart = 0;
			xstart = (myt->x - myt->rx);

			memcpy(myt->planchon + (ystart * myt->rwidth + xstart), net->planchon + (netystart * net->rwidth + netxstart), myt->width * sizeof(float));

			unlockTile(net);
		}

		if (myt->rowid != tile_rows - 1)
		{
			if (threadid == 0)
				tempiotime = startIOTime();
			tile* net = getTile(self, createTileRequestForPitRemove(t + tile_columns));
			if (threadid == 0)
				stopIOTime(tempiotime);

			netystart = (net->y - net->ry);
			netxstart = (net->x - net->rx);

			ystart = (myt->y - myt->ry) + myt->height;
			xstart = (myt->x - myt->rx);

			memcpy(myt->planchon + (ystart * myt->rwidth + xstart), net->planchon + (netystart * net->rwidth + netxstart), myt->width * sizeof(float));

			unlockTile(net);
		}

		if (myt->columnid != 0 && myt->rowid != 0)
		{
			if (threadid == 0)
				tempiotime = startIOTime();
			tile* net = getTile(self, createTileRequestForPitRemove(t - tile_columns - 1));
			if (threadid == 0)
				stopIOTime(tempiotime);

			netystart = (net->y - net->ry) + net->height - 1;
			netxstart = (net->x - net->rx) + net->width - 1;

			ystart = 0;
			xstart = 0;

			myt->planchon[ystart * myt->rwidth + xstart] = net->planchon[netystart * net->rwidth + netxstart];

			unlockTile(net);
		}

		if (myt->columnid != tile_columns - 1 && myt->rowid != 0)
		{
			if (threadid == 0)
				tempiotime = startIOTime();
			tile* net = getTile(self, createTileRequestForPitRemove(t - tile_columns + 1));
			if (threadid == 0)
				stopIOTime(tempiotime);

			netystart = (net->y - net->ry) + net->height - 1;
			netxstart = (net->x - net->rx);

			ystart = 0;
			xstart = myt->rwidth - 1;

			myt->planchon[ystart * myt->rwidth + xstart] = net->planchon[netystart * net->rwidth + netxstart];

			unlockTile(net);
		}

		if (myt->columnid != 0 && myt->rowid != tile_rows - 1)
		{
			if (threadid == 0)
				tempiotime = startIOTime();
			tile* net = getTile(self, createTileRequestForPitRemove(t + tile_columns - 1));
			if (threadid == 0)
				stopIOTime(tempiotime);

			netystart = (net->y - net->ry);
			netxstart = (net->x - net->rx) + net->width - 1;

			ystart = myt->rheight - 1;
			xstart = 0;

			myt->planchon[ystart * myt->rwidth + xstart] = net->planchon[netystart * net->rwidth + netxstart];

			unlockTile(net);
		}

		if (myt->columnid != tile_columns - 1 && myt->rowid != tile_rows - 1)
		{
			if (threadid == 0)
				tempiotime = startIOTime();
			tile* net = getTile(self, createTileRequestForPitRemove(t + tile_columns + 1));
			if (threadid == 0)
				stopIOTime(tempiotime);

			netystart = (net->y - net->ry);
			netxstart = (net->x - net->rx);

			ystart = myt->rheight - 1;
			xstart = myt->rwidth - 1;

			myt->planchon[ystart * myt->rwidth + xstart] = net->planchon[netystart * net->rwidth + netxstart];

			unlockTile(net);
		}

		unlockTile(myt);
	}

	if (InterlockedIncrement(&num_of_threads_that_exited_barrier)<thread_count)
		WaitForSingleObject(exit_semaphore,INFINITE);
	else {
		num_of_threads_that_entered_barrier = 0;
		ReleaseSemaphore(exit_semaphore,thread_count-1,&prev);
	}
}

static void processPlanchon(int threadid)
{
	const int d1[9] = { 0,1, 1, 0,-1,-1,-1,0,1};
	const int d2[9] = { 0,0,-1,-1,-1, 0, 1,1,1};

	int j,i,k,t,ct;
	long in,jn;
	LONG prev;
	float neighborFloat;
	long tilestartindex = workers[threadid]->tilestartindex;
	long tileendindex =  workers[threadid]->tileendindex;
	planchonworker* self = workers[threadid];
	timeStamp tempiotime;

	const float nodata = getDEMNoData(dem_file);

	while (true)
	{
		if (InterlockedIncrement(&num_of_threads_that_entered_barrier2)<thread_count)
			WaitForSingleObject(entrance_semaphore2,INFINITE);
		else { 
			num_of_threads_that_exited_barrier2 = 0;
			ReleaseSemaphore(entrance_semaphore2,thread_count-1,&prev);
		}

		if (threadid == 0)
		{
			globalfinished = true;
			globalfinished2 = true;
		}

		if (InterlockedIncrement(&num_of_threads_that_exited_barrier2)<thread_count)
			WaitForSingleObject(exit_semaphore2,INFINITE);
		else {
			num_of_threads_that_entered_barrier2 = 0;
			ReleaseSemaphore(exit_semaphore2,thread_count-1,&prev);
		}

		for (t = tilestartindex; t < tileendindex; t++)
		{
			if (isTileFinished(t))
			{
				continue;
			}

			if (threadid == 0)
				tempiotime = startIOTime();
			tile* myt = getTile(self, createTileRequestForPitRemove(t));
			if (threadid == 0)
				stopIOTime(tempiotime);

			while(true) 
			{
				myt->finished = true;
				while (!is_mm_stack_empty(myt->stacks[myt->whichstack]))
				{
					j = top_mm_stack(myt->stacks[myt->whichstack]);
					pop_mm_stack(myt->stacks[myt->whichstack]);
					i = top_mm_stack(myt->stacks[myt->whichstack]);
					pop_mm_stack(myt->stacks[myt->whichstack]);

					// only enter if there is data there AND there is "water" on planchon
					if(myt->planchon[j * myt->rwidth + i] != nodata && myt->planchon[j * myt->rwidth + i] > myt->map[j * myt->rwidth + i])
					{
						// find minimum elevation of the neighbouring cells
						neighborFloat = FLT_MAX;
						for(k=1; k<=8; k+=1)
						{
							in = i+d1[k];
							jn = j+d2[k];

							if(hasAccess(myt,in,jn) && myt->planchon[jn * myt->rwidth + in] < neighborFloat)
								//get neighbor data and store as planchon for self
									neighborFloat = myt->planchon[jn * myt->rwidth + in];
						}

						//set the grid to either elevDEM, all "water" can be taken off"
						if(myt->map[j * myt->rwidth + i] >= (neighborFloat + epsilon)){
							myt->planchon[j * myt->rwidth + i] = myt->map[j * myt->rwidth + i];
							myt->finished = false;
							globalfinished = false;
						}
						// or some water can be taken off
						else 
						{
							if(myt->planchon[j * myt->rwidth + i] > (neighborFloat + epsilon)){
								myt->planchon[j * myt->rwidth + i] = neighborFloat + epsilon;
								myt->finished = false;
								globalfinished = false;
							}

							push_mm_stack(myt->stacks[(myt->whichstack+1)%2], i);
							push_mm_stack(myt->stacks[(myt->whichstack+1)%2], j);
						}
					}
				}

				myt->whichstack = (myt->whichstack + 1)%2;

				if (myt->finished)
				{
					break;
				}
			}

			if (is_mm_stack_empty(myt->stacks[myt->whichstack]) && is_mm_stack_empty(myt->stacks[(myt->whichstack + 1)%2]))
				myt->finished = true;
			else
				myt->finished = false;

			unlockTile(myt);
		}

		exchangeBorders(threadid);

		for (t = tilestartindex; t < tileendindex; t++)
		{
			if (isTileFinished(t))
			{
				tileWorkloads[t] = 0;
				continue;
			}

			if (threadid == 0)
				tempiotime = startIOTime();
			tile* myt = getTile(self, createTileRequestForPitRemove(t));
			if (threadid == 0)
				stopIOTime(tempiotime);

			while(true) 
			{
				myt->finished = true;
				while (!is_mm_stack_empty(myt->stacks[myt->whichstack]))
				{
					j = top_mm_stack(myt->stacks[myt->whichstack]);
					pop_mm_stack(myt->stacks[myt->whichstack]);
					i = top_mm_stack(myt->stacks[myt->whichstack]);
					pop_mm_stack(myt->stacks[myt->whichstack]);

					// only enter if there is data there AND there is "water" on planchon
					if(myt->planchon[j * myt->rwidth + i] != nodata && myt->planchon[j * myt->rwidth + i] > myt->map[j * myt->rwidth + i])
					{
						//checks each direction...
						neighborFloat = FLT_MAX;
						for(k=1; k<=8; k+=1)
						{
							in = i+d1[k];
							jn = j+d2[k];

							if(hasAccess(myt,in,jn) && myt->planchon[jn * myt->rwidth + in] < neighborFloat)
								//get neighbor data and store as planchon for self
									neighborFloat = myt->planchon[jn * myt->rwidth + in];
						}
						//set the grid to either elevDEM, all "water" can be taken off"
						if(myt->map[j * myt->rwidth + i] >= (neighborFloat + epsilon)){
							myt->planchon[j * myt->rwidth + i] = myt->map[j * myt->rwidth + i];
							myt->finished = false;
							globalfinished2 = false;
						}
						// or some water can be taken off
						else 
						{				
							if(myt->planchon[j * myt->rwidth + i] > (neighborFloat + epsilon)){
								myt->planchon[j * myt->rwidth + i] = neighborFloat + epsilon;
								myt->finished = false;
								globalfinished2 = false;
							}

							push_mm_stack(myt->stacks[(myt->whichstack+1)%2], i);
							push_mm_stack(myt->stacks[(myt->whichstack+1)%2], j);
						}
					}
				}

				myt->whichstack = (myt->whichstack + 1)%2;

				if (myt->finished)
				{
					break;
				}
			}

			if (is_mm_stack_empty(myt->stacks[myt->whichstack]) && is_mm_stack_empty(myt->stacks[(myt->whichstack + 1)%2]))
				myt->finished = true;
			else
				myt->finished = false;

			tileWorkloads[t] = size_mm_stack(myt->stacks[0]) + size_mm_stack(myt->stacks[1]);
			unlockTile(myt);
		}

		if (InterlockedIncrement(&num_of_threads_that_entered_barrier3)<thread_count)
			WaitForSingleObject(entrance_semaphore3,INFINITE);
		else { 
			num_of_threads_that_exited_barrier3 = 0;
			ReleaseSemaphore(entrance_semaphore3,thread_count-1,&prev);
		}

		if (globalfinished && globalfinished2)
			break;

		if (InterlockedIncrement(&num_of_threads_that_exited_barrier3)<thread_count)
			WaitForSingleObject(exit_semaphore3,INFINITE);
		else {
			num_of_threads_that_entered_barrier3 = 0;
			ReleaseSemaphore(exit_semaphore3,thread_count-1,&prev);
		}

		exchangeBorders(threadid);

		// disable load balancing feature
		performLoadBalancing(threadid);
	}
}

static bool hasAccess(tile* myt, int x, int y)
{
	if(x>=0 && x<myt->rwidth && y>=0 && y<myt->rheight) 
		return true;

	return false;
}


static void runPlanchonWorkers()
{
	if (thread_count <= 0)
	{
		cerr << "There must be at least one thread!" << endl;
		exit(1);
	}

	int local_tile_count = getTileSize() / thread_count;
	tileWorkloads = (__int64*) malloc(sizeof(__int64) * getTileSize());

	int tstartindex = 0;

	int i, j, k;
	workers = (planchonworker**) malloc(thread_count * sizeof(planchonworker*));

	if (thread_count > 1)
		all_threads = (HANDLE*) malloc(sizeof(HANDLE) * (thread_count - 1));

	workers[0] = (planchonworker*) malloc(sizeof(planchonworker));
	workers[0]->threadid = 0;
	workers[0]->threadhandler = GetCurrentThread();
	workers[0]->finished = false;
	workers[0]->size = local_tile_count;
	workers[0]->lasttileid = -1;
	workers[0]->tilestartindex = tstartindex;
	workers[0]->tileendindex = tstartindex + local_tile_count;
	workers[0]->hIOEvent = CreateEvent(NULL, false, false, NULL);

	tstartindex += local_tile_count;

	for(i=1; i<thread_count; i++)
	{
		workers[i] = (planchonworker*) malloc(sizeof(planchonworker));
		workers[i]->threadid = i;
		workers[i]->lasttileid = -1;
		workers[i]->finished = false;
		workers[i]->tilestartindex = tstartindex;
		workers[i]->hIOEvent = CreateEvent(NULL, false, false, NULL);
		if (i == thread_count - 1)
		{
			workers[i]->tileendindex = getTileSize();
			workers[i]->size = workers[i]->tileendindex - workers[i]->tilestartindex;
		}
		else
		{
			workers[i]->tileendindex = tstartindex + local_tile_count;
			workers[i]->size = local_tile_count;
		}

		all_threads[i-1] = workers[i]->threadhandler = CreateThread(NULL, 0, planchonWorkerProc, (LPVOID) i, 0, NULL);

		tstartindex += local_tile_count;
	}

	planchonWorkerProc((LPVOID) 0);
}

static DWORD WINAPI planchonWorkerProc(LPVOID lpParam)
{
	int selfid = (int) lpParam;
	planchonworker* self = workers[selfid];

	initializePlanchon(selfid);
	processPlanchon(selfid);
	saveAllTiles(selfid);

	return 0;
}