/*
	Project: HydroVTMM
	Description: A User-level virtual memory system for shared-memory multithreaded systems to process large DEMs 

	HydroVTMM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Regarding this entire document or any portion of it , the author 
	makes no guarantees and is not responsible for any damage resulting 
	from its use.

	Ahmet Artu Yildirim
	Utah State University
	ahmetartu@aggiemail.usu.edu
*/

#ifndef __MEMORYMANAGER__
#define __MEMORYMANAGER__

#include <iostream>
#include <fstream>
#include "planchonworker.h"

using namespace std;

#include "tile.h"
#include "tifio.h"

int getDEMWidth(DEMFile* dem_handle);
int getDEMHeight(DEMFile* dem_handle);
float* getPlanchonBuffer(DEMFile* dem_handle);
float getDEMNoData(DEMFile* dem_handle);
DWORD getIOThreadNum();
double getHitRatio();
void finalizeTiffIO();
tile* getTile(planchonworker* worker, tile_request* request);
DEMFile* initializeMemoryManager(const char* tifpath, int maxtilewidth, int maxtileheight, long long memorylimit, char* swapdirpath, int threadcount);
void finalizeMemoryManager();
int getTileColumnSize();
int getTileRowSize();
int getTileSize();
int getMaxTileWidth();
int getMaxTileHeight();
// void increaseMemoryUsage(long long amount);
bool shouldIDoSwapOutOperation();
tile** getTiles();
bool isTileFinished(int tileID);
void performSwapOutOperation();
void swapIn (tile* tile);
void saveTile(planchonworker* worker, int tileID);
bool isMMEnabled();

#endif