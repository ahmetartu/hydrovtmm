/*
	Project: HydroVTMM
	Description: A User-level virtual memory system for shared-memory multithreaded systems to process large DEMs 

	HydroVTMM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Regarding this entire document or any portion of it , the author 
	makes no guarantees and is not responsible for any damage resulting 
	from its use.

	Ahmet Artu Yildirim
	Utah State University
	ahmetartu@aggiemail.usu.edu
*/

#include "timing.h"

double getTimeSecs(const timeStamp& start, const timeStamp& end)
{
    timeStamp dwFreq;
    QueryPerformanceFrequency(&dwFreq);
    return double(end.QuadPart - start.QuadPart) / double(dwFreq.QuadPart);
}

timeStamp getCurrentTimeStamp()
{
    timeStamp tmp;
    QueryPerformanceCounter(&tmp);
    return tmp;
}

double getCurrentTimeSecs() 
{
	 timeStamp tmp;
    QueryPerformanceCounter(&tmp);
	 timeStamp dwFreq;
    QueryPerformanceFrequency(&dwFreq);
    return (double(tmp.QuadPart) / double(dwFreq.QuadPart));
}