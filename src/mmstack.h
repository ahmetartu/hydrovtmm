/*
	Project: HydroVTMM
	Description: A User-level virtual memory system for shared-memory multithreaded systems to process large DEMs 

	HydroVTMM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Regarding this entire document or any portion of it , the author 
	makes no guarantees and is not responsible for any damage resulting 
	from its use.

	Ahmet Artu Yildirim
	Utah State University
	ahmetartu@aggiemail.usu.edu
*/

#ifndef __mmstack__
#define __mmstack__

#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;

#define INITIAL_CAPACITY 40
#define GROW_CONSTANT 1.5f

typedef struct _mmstack
{
	int* data;
	int size;
	int capacity;	
} mmstack;

mmstack*		create_mm_stack ();
void			pop_mm_stack (mmstack* stack);
int				top_mm_stack (mmstack* stack);
void			push_mm_stack (mmstack* stack, int item);
bool			is_mm_stack_empty (mmstack* stack);
int				size_mm_stack(mmstack* stack);
void			free_mm_stack (mmstack* stack);
void			free_mm_stack_data(mmstack* stack);
void			serialize_mm_stack (mmstack* stack, fstream* m_swap_file);
void			deserialize_mm_stack (mmstack* stack, fstream* m_swap_file);

#endif
