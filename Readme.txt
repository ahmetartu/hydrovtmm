**Authors**:
Ahmet Artu Yildirim (ahmetartu@aggiemail.usu.edu)

**Abstract**:
Grid digital elevation models (DEMs) are commonly used in hydrology to derive 
information related to topographically driven flow. Advances in technology 
for creating DEMs has increased their resolution and data size with the result 
that algorithms for processing them are frequently memory limited. This project 
presents a new approach to the management of memory in the parallel solution of 
hydrologic terrain processing using a user-level virtual memory system for 
shared-memory multithreaded systems. We implemented user-level virtual memory 
system for Windows 7 operating system to process large DEM datasets on the
 machine with limited memory. We used the GDAL library to enable a wide range 
of raster file formats within the implemented memory manager. We implemented 
a modified version of the Planchon and Darboux pit filling algorithm as an 
application of our tiled virtual memory manager.

**Acknowledgment**:
This research was supported by the US Army Engineer Research and Development 
Center System-Wide Water Resources Program under contract number W912HZ-11-P-0338. 
This support is gratefully acknowledged.